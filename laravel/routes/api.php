<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RekapController;
use App\Http\Controllers\GedungController;
use App\Http\Controllers\KegiatanController;
use App\Http\Controllers\PeminjamController;

use App\Http\Controllers\KebersihanController;
use App\Http\Controllers\TimController;
use App\Http\Controllers\PenjagaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('rekap', RekapController::class);
Route::resource('gedung', GedungController::class);

Route::resource('kegiatan', KegiatanController::class);
Route::resource('peminjam', PeminjamController::class);

Route::resource('kebersihan', KebersihanController::class);
Route::resource('tim', TimController::class);
Route::resource('penjaga', PenjagaController::class);

