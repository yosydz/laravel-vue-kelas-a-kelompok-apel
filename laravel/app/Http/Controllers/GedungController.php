<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Compnenent\HttpFoundation\Response;

class GedungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gedung = Gedung::all();

        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data gedung",
            'data' => $gedung
        ];

        // dd($join);
        // $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nama_gedung' => 'required',
            'kapasitas' => 'required',
            'fasilitas' => 'required',
            'luas' => 'required',
            'm_id_penjaga' => 'required',
            'm_id_tim_kebersihan' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        try{
            $gedung = Gedung::create($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil menambah data gedung",
                'data' => $gedung
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gedung = Gedung::findOrFail($id);
        // dd($request->all());

        $validator = Validator::make($request->all(),[
            'nama_gedung' => 'required',
            'kapasitas' => 'required',
            'fasilitas' => 'required',
            'luas' => 'required',
            'm_id_penjaga' => 'required',
            'm_id_tim_kebersihan' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => true,
                'message' => $validator->errors()
            ]);
        }

        try{
            $gedung->update($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil merubah data gedung",
                'data' => $gedung
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gedung = Gedung::findOrFail($id);
        // dd($request->all());

        try{
            $gedung->delete();
            $response = [
                'error' => false,
                'message' => "Berhasil menghapus data gedung",
                'data' => $gedung
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }

    }
}
