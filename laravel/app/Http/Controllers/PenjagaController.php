<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penjaga;

class PenjagaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $penjaga = Penjaga::all();

        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data penjaga",
            'data' => $penjaga
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = \Validator::make($request->all(), [
            'nama_penjaga' => 'required',
            'ktp' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'nomor_hp' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        try{
            $penjaga = Penjaga::create($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil menambah data Penjaga",
                'data' => $penjaga
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $penjaga = Penjaga::find($id);
        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data penjaga sesuai id",
            'data' => $penjaga
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $penjaga = Penjaga::findOrFail($id);
        // dd($request->all());

        $validator = \Validator::make($request->all(), [
            'nama_penjaga' => 'required',
            'ktp' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'nomor_hp' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => true,
                'message' => $validator->errors()
            ]);
        }

        try{
            $penjaga->update($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil merubah data penjaga",
                'data' => $penjaga
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $penjaga = Penjaga::findOrFail($id);
        // dd($request->all());

        try{
            $penjaga->delete();
            $response = [
                'error' => false,
                'message' => "Berhasil menghapus data Penjaga",
                'data' => $penjaga
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }
}
