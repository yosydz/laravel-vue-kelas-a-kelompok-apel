<?php

namespace App\Http\Controllers;

use App\Models\Rekap;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RekapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $join = DB::table('rekap')
            ->join('kegiatan', 'rekap.m_id_kegiatan', '=', 'kegiatan.id_kegiatan')
            ->join('peminjam', 'kegiatan.m_id_peminjam', '=', 'peminjam.id_peminjam')
            ->join('gedung', 'rekap.m_id_gedung', '=', 'gedung.id_gedung')
            ->join('tim_kebersihan', 'gedung.m_id_tim_kebersihan', '=', 'tim_kebersihan.id')
            ->join('penjaga', 'gedung.m_id_penjaga', '=', 'penjaga.id_penjaga')
            ->select('rekap.*', 'peminjam.nama_peminjam', 'gedung.nama_gedung', 'tim_kebersihan.nama_tim', 'penjaga.nama_penjaga', 'peminjam.nama_peminjam', 'kegiatan.*')
            ->get();

            $response = [
                'error' => false,
                'message' => "Berhasil mendapatkan data rekap",
                'data' => $join
            ];

        // dd($join);
        // $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'm_id_gedung' => ['required', 'numeric'],
            'm_id_kegiatan' => ['required', 'numeric']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        try{
            $rekap = Rekap::create($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil menambah data rekap",
                'data' => $rekap
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rekap = Rekap::findOrFail($id);
        // dd($request->all());

        $validator = Validator::make($request->all(),[
            'm_id_gedung' => ['required', 'numeric'],
            'm_id_kegiatan' => ['required', 'numeric']
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => true,
                'message' => $validator->errors()
            ]);
        }

        try{
            $rekap->update($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil merubah data rekap",
                'data' => $rekap
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rekap = Rekap::findOrFail($id);
        // dd($request->all());

        try{
            $rekap->delete();
            $response = [
                'error' => false,
                'message' => "Berhasil menghapus data rekap",
                'data' => $rekap
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }

    }
}
