<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gedung;
use App\Models\Tim;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class TimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tim = Tim::all();
        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data Tim",
            'data' => $tim
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = \Validator::make($request->all(), [
            'nama_tim' => 'required'
        ]);
        // $request->validate([
        //     'nama_tim' => 'required',
        // ]);
        

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        try{
            $tim = Tim::create($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil menambah data Tim",
                'data' => $tim
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tim = Tim::find($id);
        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data tim sesuai id",
            'data' => $tim
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tim = Tim::findOrFail($id);
        // dd($request->all());

        $validator = Validator::make($request->all(),[
            'nama_tim' => 'required'
        ]);
        // $request->validate([
        //     'nama_tim' => 'required'
        // ]);

        if($validator->fails()){
            return response()->json([
                'error' => true,
                'message' => $validator->errors()
            ]);
        }

        try{
            $tim->update($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil merubah data Tim",
                'data' => $tim
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tim = Tim::findOrFail($id);
        // dd($request->all());

        try{
            $tim->delete();
            $response = [
                'error' => false,
                'message' => "Berhasil menghapus data Tim",
                'data' => $tim
            ];
            return response()->json($response, 200);

        }catch(QueryException $error){
            return response()->json([
                'error' => true,
                'message' => "Gagal".$error->errorInfo
            ]);
        }
    }
}
