<?php

namespace App\Http\Controllers;

use App\Models\Kegiatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $kegiatan = Kegiatan::all();
        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data kegiatan",
            'data' => $kegiatan,
        ];

        return response()->json($response, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'nama_kegiatan' => 'required',
            'instansi' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_akhir' => 'required',
            'm_id_peminjam' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            $kegiatan = Kegiatan::create($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil menambah data kegiatan",
                'data' => $kegiatan,
            ];
            return response()->json($response, 200);

        } catch (QueryException $error) {
            return response()->json([
                'error' => true,
                'message' => "Gagal" . $error->errorInfo,
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $kegiatan = Kegiatan::find($id);
        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data kegiatan",
            'data' => $kegiatan,
        ];

        return response()->json($response, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $kegiatan = Kegiatan::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'nama_kegiatan' => 'required',
            'instansi' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_akhir' => 'required',
            'm_id_peminjam' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
            ]);
        }

        try {
            $kegiatan->update($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil merubah data kegiatan",
                'data' => $kegiatan,
            ];
            return response()->json($response, 200);

        } catch (QueryException $error) {
            return response()->json([
                'error' => true,
                'message' => "Gagal" . $error->errorInfo,
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $kegiatan = Kegiatan::findOrFail($id);

        try {
            $kegiatan->delete();
            $response = [
                'error' => false,
                'message' => "Berhasil menghapus data kegiatan",
                'data' => $kegiatan,
            ];
            return response()->json($response, 200);

        } catch (QueryException $error) {
            return response()->json([
                'error' => true,
                'message' => "Gagal" . $error->errorInfo,
            ]);
        }

    }
}
