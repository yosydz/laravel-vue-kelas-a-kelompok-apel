<?php

namespace App\Http\Controllers;

use App\Models\Peminjam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class PeminjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $peminjam = Peminjam::all();
        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data peminjam",
            'data' => $peminjam,
        ];

        return response()->json($response, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'nama_peminjam' => 'required',
            'status' => 'required',
            'nim_ktp' => 'required',
            'jenis_kelamin' => 'required',
            'nomor_hp' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            $peminjam = Peminjam::create($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil menambah data peminjam",
                'data' => $peminjam,
            ];
            return response()->json($response, 200);

        } catch (QueryException $error) {
            return response()->json([
                'error' => true,
                'message' => "Gagal" . $error->errorInfo,
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $peminjam = Peminjam::find($id);
        $response = [
            'error' => false,
            'message' => "Berhasil mendapatkan data kegiatan",
            'data' => $peminjam,
        ];

        return response()->json($response, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $peminjam = Peminjam::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'nama_peminjam' => 'required',
            'status' => 'required',
            'nim_ktp' => 'required',
            'jenis_kelamin' => 'required',
            'nomor_hp' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
            ]);
        }

        try {
            $peminjam->update($request->all());
            $response = [
                'error' => false,
                'message' => "Berhasil merubah data Peminjam",
                'data' => $peminjam,
            ];
            return response()->json($response, 200);

        } catch (QueryException $error) {
            return response()->json([
                'error' => true,
                'message' => "Gagal" . $error->errorInfo,
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $peminjam = Peminjam::findOrFail($id);

        try {
            $peminjam->delete();
            $response = [
                'error' => false,
                'message' => "Berhasil menghapus data Peminjam",
                'data' => $peminjam,
            ];
            return response()->json($response, 200);

        } catch (QueryException $error) {
            return response()->json([
                'error' => true,
                'message' => "Gagal" . $error->errorInfo,
            ]);
        }

    }
}
