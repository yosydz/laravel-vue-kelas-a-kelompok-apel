<?php

namespace App\Models;

use App\Models\Peminjam;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    protected $table = 'kegiatan';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_kegiatan';

    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_kegiatan',
        'instansi',
        'tanggal_mulai',
        'tanggal_akhir',
        'm_id_peminjam',
    ];

    public function peminjam()
    {
        return $this->belongsTo('App\Models\Peminjam', 'm_id_peminjam', 'id_peminjam');
    }
}
