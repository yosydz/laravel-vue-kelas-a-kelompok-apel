<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tim;

class Kebersihan extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = "anggota_kebersihan";
    // protected $primaryKey = 'id';

    protected $fillable = ["m_id_tim_kebersihan","nama", "ktp", "tempat_lahir", "tanggal_lahir", "jenis_kelamin", "alamat", "nomor_hp"];

    public function tim()
    {
        return $this->belongsTo(Tim::class, 'm_id_tim_kebersihan');
    }
}
