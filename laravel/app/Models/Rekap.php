<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rekap extends Model
{
    use HasFactory;

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "rekap";

    /**
     * The primary key for the model.
     *
     * @var string
     */
     protected $primaryKey = 'id_rekap';


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = ['m_id_gedung','m_id_kegiatan'];

    public $timestamps = false;
}
