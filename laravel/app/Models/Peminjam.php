<?php

namespace App\Models;

use App\Models\Kegiatan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peminjam extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'peminjam';

    /**
    * The primary key for the model.
    *
    * @var string
    */
    protected $primaryKey = 'id_peminjam';

    use HasFactory;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'nama_peminjam',
        'status',
        'nim_ktp',
        'jenis_kelamin',
        'nomor_hp',
        'email',
    ];

    public function kegiatan()
    {
        return $this->hasMany('App\Models\Kegiatan', 'm_id_peminjam');
    }

}
