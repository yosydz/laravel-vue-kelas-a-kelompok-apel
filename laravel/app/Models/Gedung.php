<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gedung extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gedung';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_gedung';

    public function penjaga()
    {
        return $this->belongsTo(Penjaga::class, 'm_id_penjaga');
    }

    public function tim()
    {
        return $this->belongsTo(Tim::class, 'm_id_tim_kebersihan');
    }

    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_gedung',
        'kapasitas',
        'fasilitas',
        'luas',
        'm_id_penjaga',
        'm_id_tim_kebersihan'
    ];
}
