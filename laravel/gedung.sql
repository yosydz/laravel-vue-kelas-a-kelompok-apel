-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2021 at 10:54 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gedung`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota_kebersihan`
--

CREATE TABLE `anggota_kebersihan` (
  `id` int(11) NOT NULL,
  `m_id_tim_kebersihan` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `ktp` varchar(20) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `nomor_hp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota_kebersihan`
--

INSERT INTO `anggota_kebersihan` (`id`, `m_id_tim_kebersihan`, `nama`, `ktp`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `nomor_hp`) VALUES
(1, 2, 'Surti', '356782821231', 'Malang', '2021-03-01', 'Perempuan', 'Malang Kota ne', '098765432123'),
(2, 2, 'Pardi', '3456378920978765', 'Pasuruan', '2021-03-10', 'Laki-laki', 'Pasuruan Desa', '085335768021'),
(3, 1, 'Rusti', '35220578291782', 'Surabaya', '2021-03-03', 'Perempuan', 'Surabaya Ngesong', '081234567098'),
(4, 1, 'Radi gaga', '345637123978765', 'Gadang', '2021-03-09', '', 'Lowokwaru', '085335768021');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gedung`
--

CREATE TABLE `gedung` (
  `id_gedung` int(11) NOT NULL,
  `nama_gedung` varchar(50) NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `fasilitas` varchar(100) NOT NULL,
  `luas` varchar(10) NOT NULL,
  `m_id_penjaga` int(11) NOT NULL,
  `m_id_tim_kebersihan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gedung`
--

INSERT INTO `gedung` (`id_gedung`, `nama_gedung`, `kapasitas`, `fasilitas`, `luas`, `m_id_penjaga`, `m_id_tim_kebersihan`) VALUES
(1, 'Gor Pertamina', 1200, 'all you can use', '200m2', 2, 2),
(2, 'Widyaloka', 890, 'all in one', '180m2', 1, 1),
(3, 'Samanta Krida', 2000, 'no one can use without permission', '400m2', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `m_id_peminjam` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `nama_kegiatan`, `instansi`, `tanggal_mulai`, `tanggal_akhir`, `m_id_peminjam`, `created_at`, `updated_at`) VALUES
(1, 'Olimpiade Brawijaya 2022', 'EM', '2021-03-08', '2021-03-29', 1, NULL, NULL),
(2, 'GBQ', 'KSR', '2021-03-08', '2021-03-13', 2, NULL, NULL),
(3, 'Liga Voli Brawijaya', 'Komunitas Volly Brawijaya', '2021-03-08', '2021-03-09', 3, NULL, NULL),
(4, 'OSPEK Mahasiswa Baru', 'BEM UB', '2021-03-01', '2021-03-04', 3, NULL, NULL),
(7, 'Ospek FILKOM 2021', 'Filkom', '2021-05-26', '2021-05-29', 3, '2021-05-19 04:20:46', '2021-05-19 17:27:02'),
(12, 'Olimpiade Sains Nasional 2022', 'Kemendikbud', '2021-05-21', '2021-05-31', 8, '2021-05-19 17:50:12', '2021-05-19 17:50:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2014_10_12_000000_create_users_table', 1),
(19, '2014_10_12_100000_create_password_resets_table', 1),
(20, '2019_08_19_000000_create_failed_jobs_table', 1),
(21, '2021_03_24_075048_create_kegiatans_table', 1),
(22, '2021_03_25_173933_create_peminjams_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `peminjam`
--

CREATE TABLE `peminjam` (
  `id_peminjam` int(11) NOT NULL,
  `nama_peminjam` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `nim_ktp` varchar(20) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `nomor_hp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjam`
--

INSERT INTO `peminjam` (`id_peminjam`, `nama_peminjam`, `status`, `nim_ktp`, `jenis_kelamin`, `nomor_hp`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Dio hilmi', 'mahasiswa', '205150709101190', 'Laki-laki', '081234567098', 'di0@gmail.com', NULL, NULL),
(2, 'Haqqi Al Haq', 'mahasiswa', '303246789811102', 'Laki-laki', '085335432712', 'haq@gmail.com', NULL, NULL),
(3, 'Hamim', 'mahasiswa', '20733727218787', 'Laki-laki', '098765432123', 'hamim@gmail.com', NULL, NULL),
(4, 'Ahmadi Subhan', 'Staff', '5475675745', 'Perempuan', '65065406403515', 'adbul@gmail.com', '2021-05-19 01:29:30', '2021-05-19 17:02:10'),
(8, 'Hamim Nizar Y', 'Mahasiswa', '35220582939229', 'Laki-laki', '098122332919', 'hamim@gmail.com', '2021-05-19 17:49:43', '2021-05-19 17:49:43');

-- --------------------------------------------------------

--
-- Table structure for table `penjaga`
--

CREATE TABLE `penjaga` (
  `id_penjaga` int(11) NOT NULL,
  `nama_penjaga` varchar(100) NOT NULL,
  `ktp` varchar(20) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `nomor_hp` varchar(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjaga`
--

INSERT INTO `penjaga` (`id_penjaga`, `nama_penjaga`, `ktp`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `nomor_hp`, `updated_at`, `created_at`) VALUES
(1, 'Suparman', '3522057829109992', 'Malang', '2021-03-16', 'Laki-laki', 'Dinoyo', '081234567098', '2021-05-19 00:26:00', '2021-05-17 00:25:57'),
(2, 'Rapasodi', '3456378920978765', 'Batu', '2021-03-09', 'Laki-laki', 'Batu night spectacular', '085335432712', '2021-05-19 00:26:19', '2021-05-26 00:26:11'),
(4, 'Budi Sudarsoono', '3587723', 'Mjk', '1981-10-12', 'lakilaki', 'mojokerto', '08127871', '2021-05-18 17:27:08', '2021-05-18 17:27:08');

-- --------------------------------------------------------

--
-- Table structure for table `rekap`
--

CREATE TABLE `rekap` (
  `id_rekap` int(11) NOT NULL,
  `m_id_gedung` int(11) NOT NULL,
  `m_id_kegiatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekap`
--

INSERT INTO `rekap` (`id_rekap`, `m_id_gedung`, `m_id_kegiatan`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3),
(5, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tim_kebersihan`
--

CREATE TABLE `tim_kebersihan` (
  `id` int(11) NOT NULL,
  `nama_tim` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tim_kebersihan`
--

INSERT INTO `tim_kebersihan` (`id`, `nama_tim`) VALUES
(1, 'Pejuang Suci'),
(2, 'Anti Kotor');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Admin', 'admin@argon.com', '2021-03-25 11:09:37', '$2y$10$gbx10STIUzyr/MPUOcuVr.PA2qSJnP8L5y93swpPIyNb/gCLdSHSu', NULL, '2021-03-25 11:09:37', '2021-03-25 11:09:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota_kebersihan`
--
ALTER TABLE `anggota_kebersihan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_id_tim_kebersihan` (`m_id_tim_kebersihan`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `gedung`
--
ALTER TABLE `gedung`
  ADD PRIMARY KEY (`id_gedung`),
  ADD KEY `m_id_penjaga` (`m_id_penjaga`),
  ADD KEY `m_id_tim_kebersihan` (`m_id_tim_kebersihan`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`),
  ADD KEY `m_id_peminjam` (`m_id_peminjam`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `peminjam`
--
ALTER TABLE `peminjam`
  ADD PRIMARY KEY (`id_peminjam`);

--
-- Indexes for table `penjaga`
--
ALTER TABLE `penjaga`
  ADD PRIMARY KEY (`id_penjaga`);

--
-- Indexes for table `rekap`
--
ALTER TABLE `rekap`
  ADD PRIMARY KEY (`id_rekap`),
  ADD KEY `m_id_gedung_highlight` (`m_id_gedung`),
  ADD KEY `m_id_peminjam` (`m_id_kegiatan`);

--
-- Indexes for table `tim_kebersihan`
--
ALTER TABLE `tim_kebersihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota_kebersihan`
--
ALTER TABLE `anggota_kebersihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gedung`
--
ALTER TABLE `gedung`
  MODIFY `id_gedung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `peminjam`
--
ALTER TABLE `peminjam`
  MODIFY `id_peminjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `penjaga`
--
ALTER TABLE `penjaga`
  MODIFY `id_penjaga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rekap`
--
ALTER TABLE `rekap`
  MODIFY `id_rekap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tim_kebersihan`
--
ALTER TABLE `tim_kebersihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anggota_kebersihan`
--
ALTER TABLE `anggota_kebersihan`
  ADD CONSTRAINT `anggota_kebersihan_ibfk_1` FOREIGN KEY (`m_id_tim_kebersihan`) REFERENCES `tim_kebersihan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gedung`
--
ALTER TABLE `gedung`
  ADD CONSTRAINT `gedung_ibfk_1` FOREIGN KEY (`m_id_penjaga`) REFERENCES `penjaga` (`id_penjaga`),
  ADD CONSTRAINT `gedung_ibfk_2` FOREIGN KEY (`m_id_tim_kebersihan`) REFERENCES `tim_kebersihan` (`id`);

--
-- Constraints for table `rekap`
--
ALTER TABLE `rekap`
  ADD CONSTRAINT `rekap_ibfk_1` FOREIGN KEY (`m_id_gedung`) REFERENCES `gedung` (`id_gedung`),
  ADD CONSTRAINT `rekap_ibfk_2` FOREIGN KEY (`m_id_kegiatan`) REFERENCES `kegiatan` (`id_kegiatan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
