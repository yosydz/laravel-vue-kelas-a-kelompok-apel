import { createRouter, createWebHashHistory } from "vue-router";

import DashboardLayout from "@/layout/DashboardLayout";
import AuthLayout from "@/layout/AuthLayout";

import Dashboard from "../views/Dashboard.vue";
import Icons from "../views/Icons.vue";
import Maps from "../views/Maps.vue";
import Profile from "../views/UserProfile.vue";
import Tables from "../views/Tables.vue";
import Penjaga from "../views/Penjaga.vue";
import Gedung from "../views/Gedung.vue";
import Kebersihan from "../views/Kebersihan.vue";
import Kegiatan from "../views/Kegiatan.vue";
import Peminjam from "../views/Peminjam.vue";
import TimBersih from "../views/TimBersih.vue";
import Rekap from "../views/Rekap.vue";

import TambahPeminjam from "../views/Peminjam/Tambah.vue";
import UpdatePeminjam from "../views/Peminjam/Update.vue";

import TambahPenjaga from "../views/Penjaga/Tambah.vue";
import UpdatePenjaga from "../views/Penjaga/Update.vue";

import TambahTimBersih from "../views/Timbersih/Tambah.vue";
import UpdateTimBersih from "../views/Timbersih/Update.vue";

import TambahGedung from "../views/Gedung/Tambah.vue";
import UpdateGedung from "../views/Gedung/Update.vue";

import TambahKebersihan from "../views/Kebersihan/Tambah.vue";
import UpdateKebersihan from "../views/Kebersihan/Update.vue";

import TambahKegiatan from "../views/Kegiatan/Tambah.vue";
import UpdateKegiatan from "../views/Kegiatan/Update.vue";

import TambahRekap from "../views/Rekap/Tambah.vue";
import UpdateRekap from "../views/Rekap/Update.vue";

import Login from "../views/Login.vue";
import Register from "../views/Register.vue";

const routes = [
  {
    path: "/",
    redirect: "/dashboard",
    component: DashboardLayout,
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        components: { default: Dashboard },
      },
      {
        path: "/penjaga",
        name: "penjaga",
        components: { default: Penjaga },
      },
      {
        path: "/penjaga/tambah",
        name: "tambah penjaga",
        components: { default: TambahPenjaga },
      },
      {
        path: "/penjaga/update/:id_penjaga",
        name: "update penjaga",
        components: { default: UpdatePenjaga },
      },


      {
        path: "/gedung",
        name: "gedung",
        components: { default: Gedung },
      },
      {
        path: "/gedung/tambah",
        name: "tambah gedung",
        components: { default: TambahGedung },
      },
      {
        path: "/gedung/update",
        name: "update gedung",
        components: { default: UpdateGedung },
      },


      {
        path: "/kebersihan",
        name: "kebersihan",
        components: { default: Kebersihan },
      },
      {
        path: "/kebersihan/tambah",
        name: "tambah kebersihan",
        components: { default: TambahKebersihan },
      },
      {
        path: "/kebersihan/update/:id",
        name: "update kebersihan",
        components: { default: UpdateKebersihan },
      },
      {
        path: "/timbersih",
        name: "tim bersih",
        components: { default: TimBersih },
      },
      {
        path: "/timbersih/tambah",
        name: "tambah tim bersih",
        components: { default: TambahTimBersih },
      },
      {
        path: "/timbersih/update/:id",
        name: "update tim bersih",
        components: { default: UpdateTimBersih },
      },
      {
        path: "/kegiatan",
        name: "kegiatan",
        components: { default: Kegiatan },
      },
      {
        path: "/kegiatan/tambah",
        name: "tambah kegiatan",
        components: { default: TambahKegiatan },
      },
      {
        path: "/kegiatan/update/:id_kegiatan",
        name: "update kegiatan",
        components: { default: UpdateKegiatan },
      },
      {
        path: "/peminjam",
        name: "peminjam",
        components: { default: Peminjam },
      },
      {
        path: "/peminjam/tambah",
        name: "tambah peminjam",
        components: { default: TambahPeminjam },
      },
      {
        path: "/peminjam/update/:id_peminjam",
        name: "update peminjam",
        components: { default: UpdatePeminjam },
      },
      {
        path: "/rekap",
        name: "rekapitulasi",
        components: { default: Rekap },
      },
      {
        path: "/rekap/tambah",
        name: "Tambah Rekap",
        components: { default: TambahRekap },
      },
      {
        path: "/rekap/update/:id_rekap",
        name: "Update Rekap",
        components: { default: UpdateRekap },
      },
      {
        path: "/icons",
        name: "icons",
        components: { default: Icons },
      },
      {
        path: "/maps",
        name: "maps",
        components: { default: Maps },
      },
      {
        path: "/profile",
        name: "profile",
        components: { default: Profile },
      },
      {
        path: "/tables",
        name: "tables",
        components: { default: Tables },
      },
    ],
  },
  {
    path: "/",
    redirect: "login",
    component: AuthLayout,
    children: [
      {
        path: "/login",
        name: "login",
        components: { default: Login },
      },
      {
        path: "/register",
        name: "register",
        components: { default: Register },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  linkActiveClass: "active",
  routes,
});

export default router;
